import Vue from 'vue'
import Router from 'vue-router'
import SpellChooser from "../components/spell/SpellChooser";
import SpellbookChooser from "../components/spellbook/SpellbookChooser";

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/spell',
            name: 'spell',
            component: SpellChooser,
            props: true
        },
        {
            path: '/spellbook',
            name: 'spellbook',
            component: SpellbookChooser,
            props: true
        }
    ]
})